# Tunnel

First set variables `SERVERURL`, `WEBPASSWORD` and `DEFAULT_HOST`
of the `docker-compose.yml` file

To Generate containers:

```console
$ docker-compose -f path/to/docker-compose.yml up -d
```

or

```console
$ docker-compose up -d
```

To see the QR code generated:

```console
$ docker exec -it wireguard /app/show-peer 1
```

To add more Peers

```
$ docker exec -it wireguard /app/add-peer
```

## Fix permission share volumes

Hyperbola:

```console
$ chown -R nobody:nobody dnscrypt-proxy
```

```console
$ chmod 775 dnscrypt-proxy/cache
```

Debian:

```console
$ chown -R nobody:nogroup dnscrypt-proxy
```

## Troubleshooting

```console
export COMPOSE_HTTP_TIMEOUT=600
export DOCKER_CLIENT_TIMEOUT=5000
```
